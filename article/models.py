from django.db import models

class Article(models.Model):
    title = models.CharField(max_length=100)
    author = models.CharField(max_length=100)
    text = models.TextField()
    image = models.CharField(max_length=255)

    def __str__(self):
        return self.title


class Tags(models.Model):
    name = models.CharField(max_length=100)
    article = models.ForeignKey(
        Article,
        on_delete=models.CASCADE,
        related_name='tags'
    )

    def __str__(self):
        return self.name
