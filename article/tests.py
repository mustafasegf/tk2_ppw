from django.test import TestCase, Client
from django.urls.base import resolve
from .models import Article, Tags
from django.urls import reverse

# Create your tests here.


class TestCaseArticle(TestCase):

    def test_check_article_models(self):
        Article.objects.create(
            title='Judul',
            author='Penulis',
            text='Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
            image='example.com/image.png'
        )
        article = Article.objects.get(title='Judul')
        self.assertEqual('Judul', article.title)
        self.assertEqual('Penulis', article.author)
        self.assertIn('Lorem ipsum', article.text)
        self.assertEqual('example.com/image.png', article.image)
        self.assertIn(str(article), article.title)

    def test_check_tags_models(self):
        article_create = Article.objects.create(
            title='Judul',
            author='Penulis',
            text='Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
            image='example.com/image.png'
        )
        article_create.tags.create(name='tag1')
        article_create.tags.create(name='tag2')

        article = Article.objects.get(title='Judul')
        tags = article.tags.all()
        self.assertEqual(2, len(tags))

        self.assertEqual('tag1', tags[0].name)
        self.assertIn(str(tags[0]), tags[0].name)

        self.assertEqual('tag2', tags[1].name)
        self.assertIn(str(tags[1]), tags[1].name)

    def test_url_root(self):
        response = Client().get(reverse('article:index'))
        self.assertEqual(200, response.status_code)
        self.assertIn('Article Corona Terbaru', str(response.content))

    def test_url_article(self):
        Article.objects.create(
            title='Judul',
            author='Penulis',
            text='Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
            image='example.com/image.png'
        )
        response = Client().get(reverse('article:detail', kwargs={'id': 1}))
        self.assertEqual(200, response.status_code)
        self.assertIn('Judul', str(response.content))

    def test_url_tags(self):
        article_create = Article.objects.create(
            title='Judul',
            author='Penulis',
            text='Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
            image='example.com/image.png'
        )
        article_create.tags.create(name='tag1')
        response = Client().get(
            reverse('article:tags', kwargs={'name': 'tag1'}))
        self.assertEqual(200, response.status_code)
        self.assertIn('tag1', str(response.content))

    def test_url_register(self):
        response = Client().get(reverse('article:register'))
        self.assertEqual(200, response.status_code)
        self.assertIn('Register Article', str(response.content))

    def test_register(self):
        response = Client().post(reverse('article:register'), {
            'title': 'title1',
            'author': 'author1',
            'text': 'aaaaaaaaaaa',
            'image': 'example.com/image.png'
        }, follow=True)
        self.assertEqual(response.status_code, 200)

    def test_url_register_tags(self):
        response = Client().get(reverse('article:register_tags'))
        self.assertEqual(200, response.status_code)
        self.assertIn('Register Article Tags', str(response.content))

    def test_register_tags(self):
        Article.objects.create(
            title='Judul',
            author='Penulis',
            text='Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
            image='example.com/image.png'
        )
        response = Client().post(reverse('article:register_tags'), {
            'name': 'people 1',
            'article': 1,
        }, follow=True)
        self.assertEqual(response.status_code, 200)
    
    def test_api_article(self):
        article_create = Article.objects.create(
            title='Judul',
            author='Penulis',
            text='Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
            image='example.com/image.png'
        )
        article_create.tags.create(name='tag1')
        response = Client().get(reverse('article:api_article'), follow=True)
        self.assertIn('title', response.content.decode('utf-8').lower())

        response = Client().get(reverse('article:api_article')+'?id=1', follow=True)
        self.assertIn('title', response.content.decode('utf-8').lower())

    def test_api_tags(self):
        article_create = Article.objects.create(
            title='Judul',
            author='Penulis',
            text='Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
            image='example.com/image.png'
        )
        article_create.tags.create(name='tag1')
        response = Client().get(reverse('article:api_tags') + '?name=tag1', follow=True)
        self.assertIn('tag1', response.content.decode('utf-8').lower())