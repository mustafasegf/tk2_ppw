from django.forms import ModelForm
from .models import Article, Tags


class ArticleForm(ModelForm):
    class Meta:
        model = Article
        fields = ['title', 'author', 'text', 'image']

class TagsForm(ModelForm):
    class Meta:
        model = Tags
        fields = ['name', 'article']

