from django.urls import path
from . import views

app_name = 'article'

urlpatterns = [
    path('', views.index, name="index"),
    path('<int:id>', views.detail, name="detail"),
    path('tags/<str:name>', views.tags, name="tags"),
    path('register', views.register, name="register"),
    path('register_tags', views.register_tags, name="register_tags"),
    path('api', views.api_article, name="api_article"),
    path('api/tags', views.api_tags, name="api_tags"),
]
