from article.models import Article, Tags
from django.contrib import admin

# Register your models here.
admin.site.register(Article)
admin.site.register(Tags)