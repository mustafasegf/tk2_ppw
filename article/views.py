from django.http import request, JsonResponse
from django.shortcuts import redirect, render
from .models import Article, Tags
from .forms import ArticleForm, TagsForm


def index(request):
    context = {'page_title': 'Article Corona Terbaru'}
    return render(request, "article/index.html", context)


def detail(request, id):
    article = Article.objects.get(id=id)
    context = {'page_title': article.title, 'id': id}
    return render(request, "article/detail.html", context)


def tags(request, name):
    context = {'page_title': name, 'name':name}
    return render(request, "article/tags.html", context)


def register(request):
    form_article = ArticleForm()
    if request.method == 'POST':
        form_article = ArticleForm(request.POST)
        if form_article.is_valid():
            form_article.save()
        return redirect('article:index')
    context = {'page_title': 'register', 'form_article': form_article}
    return render(request, "article/register.html", context)


def register_tags(request):
    form_tags = TagsForm()
    if request.method == 'POST':
        form_tags = TagsForm(request.POST)
        if form_tags.is_valid():
            data = [x.strip() for x in request.POST['name'].split(',')]
            request.POST._mutable = True
            for i in data:
                request.POST['name'] = i
                TagsForm(request.POST).save()
        return redirect('article:index')
    context = {'page_title': 'register', 'form_tags': form_tags}
    return render(request, "article/register_tags.html", context)


def api_article(request):
    data = []
    articles = None
    if 'id' in request.GET:
        id = request.GET['id']
        articles = Article.objects.filter(id=id)
    else:
        articles = Article.objects.all()
    for article in articles:
        tags = []
        for tag in Tags.objects.filter(article=article.id):
            tags.append(str(tag))
        dct = {k:v for k, v in article.__dict__.items() if not k.startswith('_')}
        dct['tags'] = tags
        data.append(dct)
    return JsonResponse(data, json_dumps_params={'indent': 2}, safe=False)


def api_tags(request):
    name = request.GET['name']
    data = []
    for tag in Tags.objects.filter(name=name):
        article = Article.objects.get(id=tag.article_id)
        article = {k:v for k, v in article.__dict__.items() if not k.startswith('_')}
        tags = []
        for inner_tag in Tags.objects.filter(article=article['id']):
            tags.append(str(inner_tag))
        article.update({'tags': tags})
        data.append(article)
    return JsonResponse(data, json_dumps_params={'indent': 2}, safe=False)
