const loadArticle = (param = undefined, type) => {
  let p = {}
  if (type === 'tags') {
    p = { 'name': param}
  } else {
    p = { 'id': param }
  }

  $.ajax({
    url: location.origin + '/article/api' + (type === 'tags' ? '/tags' : ''),
    contentType: 'application/json',
    data: p,
    dataType: 'json',
    success: result => changeInner(result, type)
  })
}

const changeInner = (result, type) => {
  console.log(result, type)
  let parrent = $('.article')
  let inner = ''
  result.forEach((result) => {
    tags = ''
    result.tags.forEach((tag) => {
      tags += `<a href="${location.origin}/article/tags/${tag}" class="tags">${tag}</a>`
    })
    inner += `<div class="child">
                <h2> ${result.title} </h2>
                <h4> ${result.author} </h4>
                <p>${tags}</p>
                <img class="${type == 'article' ? 'big-thumb': 'thumbnail' }" src="${result.image}"/>`

    if (type !== 'article') {
      inner += '<p>' + truncateString(result.text, 255) + '</p>'
      inner += `<a href="${location.origin}/article/${result.id}">See more</a>`
    } else {
      inner += '<p>' + result.text.replace(/\n/g, "<br />") + '</p>'
    }
    inner += '</div>'
  })
  parrent.html(inner)
}

const truncateString = (str, length) => {
  return str === undefined ? '-' : str.length > length ? str.substring(0, length - 3) + '...' : str;
}


$('.btn').mouseover(function (){
  console.log($(this))
  $(this).css('background-color', '#003cff')
})

$('.btn').mouseout(function (){
  console.log($(this))
  $(this).css('background-color', '#007bff')
})

$('.article').on('click' , 'img', function (){
  $(this).toggleClass('big')
})