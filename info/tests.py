from django.contrib.auth.models import User
from django.test import TestCase, Client
from django.urls import reverse

from .models import KasusProvinsi, RumahSakitRujukan
from .helper import kasus_provinsi_helper, rumah_sakit_helper


class UrlsPathTest(TestCase):
    def setUp(self):
        self.info_page = reverse('info:info')

    def test_info_page_matching_with_the_right_path(self):
        self.assertEqual(self.info_page, '/info/')


class InfoPageTest(TestCase):
    def setUp(self):
        self.info_page = reverse('info:info')
        self.user = User.objects.create_user(
            username='testuser',
            email= 'test@email.com',
            password='12345678'
        )
        self.login_page = reverse('login_register:login')
    
    def test_info_page_exist(self):
        c = Client()

        # Cek, dia harus login dulu baru bisa akses info
        c.post(self.login_page, {
            'username':'testuser',
            'password':'12345678'
        }, follow=True)
        response = c.get(self.info_page)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'info/index.html')
        self.assertContains(response, 'COVID-19 Cases in Indonesia')
    
    # Tes kalau misalnya belum login
    def test_info_page_without_login(self):
        c = Client()
        response = c.get(self.info_page, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'registration/login.html')
        self.assertContains(response, 'Login')


class ModelsKasusProvinsiTest(TestCase):
    def setUp(self):
        KasusProvinsi.objects.create(
            provinsi = "DKI Jakarta",
            kasus_positif = 50,
            kasus_sembuh = 1,
            kasus_meninggal = 2
        )
        RumahSakitRujukan.objects.create(
            rumah_sakit = "RumahSakit1",
            alamat = "Alamat1",
            region = "Region1",
            nomor_telepon = "Nomor1",
            provinsi = "Provinsi1"
        )

    def test_models_true(self):
        kasus_jakarta = KasusProvinsi.objects.get(provinsi = "DKI Jakarta")
        self.assertEqual(KasusProvinsi.objects.count(), 1)
        self.assertEqual(str(kasus_jakarta), "DKI Jakarta")
        self.assertEqual(kasus_jakarta.kasus_positif, 50)
        self.assertEqual(kasus_jakarta.kasus_sembuh, 1)
        self.assertEqual(kasus_jakarta.kasus_meninggal, 2)
        kasus_jakarta.delete()
        self.assertEqual(KasusProvinsi.objects.count(), 0)

    def test_models_as_dict(self):
        kasus_jakarta = KasusProvinsi.objects.get(provinsi = "DKI Jakarta").as_dict()
        self.assertEqual(kasus_jakarta['provinsi'], "DKI Jakarta")
        self.assertEqual(kasus_jakarta['kasus_positif'], 50)
        self.assertEqual(kasus_jakarta['kasus_sembuh'], 1)
        self.assertEqual(kasus_jakarta['kasus_meninggal'], 2)
    
    def test_models_rumah_sakit_dict(self):
        rumah_sakit = RumahSakitRujukan.objects.get(provinsi = 'Provinsi1').as_dict()
        self.assertEqual(rumah_sakit['provinsi'], 'Provinsi1')
        self.assertEqual(rumah_sakit['alamat'], 'Alamat1')
        self.assertEqual(rumah_sakit['nomor_telepon'], 'Nomor1')
        
class APICallTestCase(TestCase):
    def setUp(self) -> None:
        # self.info_page = reverse('info:info')
        KasusProvinsi.objects.create(
            provinsi = "DKI Jakarta",
            kasus_positif = 50,
            kasus_sembuh = 1,
            kasus_meninggal = 2
        )

        RumahSakitRujukan.objects.create(
            rumah_sakit = "RumahSakit1",
            alamat = "Alamat1",
            region = "Region1",
            nomor_telepon = "Nomor1",
            provinsi = "dki jakarta"
        )

        self.search_page = '/info/search_cases/' + 'dki jakarta'
        self.search_hospitals = '/info/search_hospitals/' + 'dki jakarta'
    
    def test_search_call(self):
        # self.client.get(self.info_page)
        response = self.client.get(self.search_page, follow=True) 
        self.assertEqual(response.status_code, 200)
    
    def test_search_hospitals_call(self):
        response = self.client.get(self.search_hospitals, follow=True)
        self.assertEqual(response.status_code, 200)

class InfoRumahSakitTest(TestCase):
    def setUp(self):
        self.hospital_page = reverse('info:daftar_rumah_sakit')
        self.user = User.objects.create_user(
            username='testuser',
            email= 'test@email.com',
            password='12345678'
        )
        self.login_page = reverse('login_register:login')


    def test_hospitals_page_matching_with_the_right_path(self):
        self.assertEqual(self.hospital_page, '/info/rumah_sakit/')

    def test_hospital_page_exist(self):
        c = Client()

        # Cek, harus login dulu baru bisa akses list rumah
        c.post(self.login_page, {
            'username':'testuser',
            'password':'12345678'
        }, follow=True)
        response = c.get(self.hospital_page)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'info/rumah_sakit.html')
        self.assertContains(response, 'Special Hospitals for COVID-19 Patients')
    
    def test_hospital_page_without_login(self):
        c = Client()
        response = c.get(self.hospital_page , follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'registration/login.html')
        self.assertContains(response, 'Login')


class RumahSakitRujukanTest(TestCase):
    def setUp(self):
        RumahSakitRujukan.objects.create(
            rumah_sakit = "RumahSakit1",
            alamat = "Alamat1",
            region = "Region1",
            nomor_telepon = "Nomor1",
            provinsi = "Provinsi1"
        )
        self.user = User.objects.create_user(
            username='testuser',
            email= 'test@email.com',
            password='12345678'
        )
        self.login_page = reverse('login_register:login')

    def test_models_true(self):
        rumah_sakit1 = RumahSakitRujukan.objects.get(rumah_sakit = "RumahSakit1")
        self.assertEqual(RumahSakitRujukan.objects.count(), 1)
        self.assertEqual(str(rumah_sakit1), "RumahSakit1")
        self.assertEqual(rumah_sakit1.alamat, "Alamat1")
        self.assertEqual(rumah_sakit1.region, "Region1")
        self.assertEqual(rumah_sakit1.nomor_telepon, "Nomor1")
        self.assertEqual(rumah_sakit1.provinsi, "Provinsi1")
        rumah_sakit1.delete()
        self.assertEqual(RumahSakitRujukan.objects.count(), 0)


class HelperFunctionTest(TestCase):
    def setUp(self):
        KasusProvinsi.objects.create(
            provinsi = "DKI Jakarta",
            kasus_positif = 50,
            kasus_sembuh = 1,
            kasus_meninggal = 2
        )

        RumahSakitRujukan.objects.create(
            rumah_sakit = "RS UMUM DAERAH  DR. ZAINOEL ABIDIN",
            alamat = "JL. TGK DAUD BEUREUEH, NO. 108 B. ACEH",
            region = "KOTA BANDA ACEH, ACEH",
            nomor_telepon = "(0651) 34565",
            provinsi = "Aceh"
        )

        self.data = [{"attributes":{"FID":11,"Kode_Provi":31,"Provinsi":"DKI Jakarta","Kasus_Posi":107229,"Kasus_Semb":95783,"Kasus_Meni":2288}}]
        self.data_rumah_sakit = [
            {
                "name": "RS UMUM DAERAH  DR. ZAINOEL ABIDIN",
                "address": "JL. TGK DAUD BEUREUEH, NO. 108 B. ACEH",
                "region": "KOTA BANDA ACEH, ACEH",
                "phone": "(0651) 34565",
                "province": "(0651) 34565"
            },
            {
                "name": "RS UMUM DAERAH CUT MEUTIA KAB. ACEH UTARA",
                "address": "JL. BANDA ACEH-MEDAN KM.6 BUKET RATA LHOKSEUMAWE",
                "region": "KOTA LHOKSEUMAWE, ACEH",
                "phone": "(0645) 46334",
                "province": "Aceh"
            },
        ]

    def test_kasus_provinsi_helper(self):
        kasus_provinsi_helper(self.data)
        kasus_jakarta = KasusProvinsi.objects.get(
            provinsi = "DKI Jakarta"
        )
        self.assertEqual(kasus_jakarta.kasus_positif, 107229)
        self.assertEqual(kasus_jakarta.kasus_sembuh, 95783)
        self.assertEqual(kasus_jakarta.kasus_meninggal, 2288)
    
    def test_rumah_sakit_helper(self):
        rumah_sakit_helper(self.data_rumah_sakit)
        rumah_sakit_test = RumahSakitRujukan.objects.get(
            rumah_sakit = "RS UMUM DAERAH CUT MEUTIA KAB. ACEH UTARA"
        )
        self.assertEqual(rumah_sakit_test.rumah_sakit, "RS UMUM DAERAH CUT MEUTIA KAB. ACEH UTARA")
        self.assertEqual(rumah_sakit_test.alamat, "JL. BANDA ACEH-MEDAN KM.6 BUKET RATA LHOKSEUMAWE")
        self.assertEqual(rumah_sakit_test.region, "KOTA LHOKSEUMAWE, ACEH")
        self.assertEqual(rumah_sakit_test.nomor_telepon, "(0645) 46334")
        self.assertEqual(rumah_sakit_test.provinsi, "Aceh")
