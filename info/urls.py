from django.urls import path
from . import views

app_name = 'info'

urlpatterns = [
    path('', views.index, name='info'),
    path('rumah_sakit/', views.daftar_rumah_sakit, name="daftar_rumah_sakit"),

    # 
    path('search_cases/<str:query>/', views.search_cases, name='search_cases'),
    path('search_hospitals/<str:query>/', views.search_hospitals, name='search_hospitals'),
]
