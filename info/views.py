from django.http.response import JsonResponse
import requests

from django.contrib import messages
from django.shortcuts import render, redirect

from .models import KasusProvinsi, RumahSakitRujukan
from .helper import rumah_sakit_helper, kasus_provinsi_helper


def index(request):
    if(not request.user.is_authenticated):
        return redirect('login_register:login')
    response = requests.get('https://api.kawalcorona.com/indonesia/provinsi')
    data = response.json()
    kasus_provinsi_helper(data)
    # data_kasus = KasusProvinsi.objects.all()
    return render(request, 'info/index.html')


def daftar_rumah_sakit(request):
    if(not request.user.is_authenticated):
        return redirect('login_register:login')
    response = requests.get('https://dekontaminasi.com/api/id/covid19/hospitals')
    data = response.json()
    rumah_sakit_helper(data)
    # daftar_semua_rumah_sakit = RumahSakitRujukan.objects.all()
    return render(request, "info/rumah_sakit.html")

def search_cases(request, query):
    kasus_provinsi = KasusProvinsi.objects.filter(provinsi__icontains=query)
    response_json = {'kasus':[]}
    for kasus in kasus_provinsi:
        response_json['kasus'].append(kasus.as_dict())
    return JsonResponse(response_json)

def search_hospitals(request, query):
    hospitals = RumahSakitRujukan.objects.filter(provinsi__icontains=query)
    response_json = {'rs':[]}
    for rs in hospitals:
        response_json['rs'].append(rs.as_dict())
    return JsonResponse(response_json)