from django.contrib import admin
from news.models import News, HoaxReport # Outlet

# Register your models here.

# admin.site.register(Outlet)
admin.site.register(News)
admin.site.register(HoaxReport)
