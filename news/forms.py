from django import forms
from django.forms import ModelForm, Textarea
from .models import HoaxReport


class HoaxForm(ModelForm):
    """
    Form for Hoax submission
    """
    text = forms.CharField(widget=forms.Textarea)
    message = forms.CharField(widget=forms.Textarea)
    
    class Meta:
        model = HoaxReport
        fields = ['judul', 'text', 'url', 'message']
        widgets = {
            'text': Textarea(),
            'message': Textarea(),
        }
