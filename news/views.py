import re
from django.http import JsonResponse, HttpResponse
from django.core import serializers, paginator
from django.shortcuts import render
from urllib3 import PoolManager
from bs4 import BeautifulSoup
from .models import News
from .forms import HoaxForm

uri = re.compile(r"^(?:https?:\/\/)?(?:[^@\/\n]+@)?(?:www\.)?([^:\/?\n]+)")
news = None

# Create your views here.


def index(request):
    request.session['page'] = 1
    return render(request, "news/news-index.html")


def fetch(request):
    page = dict(request.GET)

    if not News.objects.filter(title=page["title"][0]).exists():
        outlet_name = uri.match(page["url"][0])[1]
        desc, img = getMeta(page["url"][0])

        news, _ = News.objects.get_or_create(
            title=page["title"][0],
            url=page["url"][0],
            unix_time=str(page["timestamp"][0]),
            description=desc,
            img=img,
            outlet=outlet_name,
        )
        news.save()
        return HttpResponse('Added')
        # outlet = Outlet.objects.get_or_create(name)
    return HttpResponse('Skipped')


def getBulk(request):
    global news

    if (news == None):
        news = paginator.Paginator(News.objects.order_by('-unix_time'), 5)

    page_num = request.session.get('page')
    if not page_num:
        page_num = 1

    if (news.num_pages >= page_num):
        try:
            ret_news = news.page(page_num)
            page_num += 1
            request.session['page'] = page_num
            return HttpResponse(serializers.serialize("json", list(ret_news.object_list)), content_type="application/json") # Manually serialize
        except:
            return JsonResponse({'error': 'true'}, safe=False)

    return JsonResponse({'empty': 'true'}, safe=False)


def hoax(request):
    context = {}
    if request.method == 'POST':
        form = context['form'] = HoaxForm(request.POST)
        if form.is_valid():
            form = form.save(commit=False)
            form.user = request.user
            context['success'] = True
            form.save()

    else:
        context['form'] = HoaxForm()

    return render(request, "news/form.html", context=context)


def getMeta(url: str):
    """
    Helper function to fetch metadata
    """
    response = PoolManager().request('GET', url)
    soup = BeautifulSoup(response.data, features="html.parser")

    desc = soup.find("meta",  property="og:description")
    img = soup.find("meta", property="og:image")

    desc = desc["content"] if desc else None
    img = img["content"] if img else None

    return desc, img
