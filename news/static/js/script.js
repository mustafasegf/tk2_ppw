$(() => {
  $.ajax({
    type: "GET",
    url: "https://cors-anywhere.herokuapp.com/https://dekontaminasi.com/api/id/covid19/news",
    success: serialize,
  }).fail(() => {
    $.ajax({
      type: "GET",
      url: "https://cors-proxy.htmldriven.com/?url=https://dekontaminasi.com/api/id/covid19/news",
      success: serialize,
    })
  });
});

function serialize(data) {
  data = JSON.parse(data)
  data.forEach(news => {
    $.ajax({
      type: "GET",
      url: "/news/api/fetch",
      data: { title: news.title, url: news.url, timestamp: news.timestamp },
      success: function (response) {
        console.log("success push data model - " + response)
      }
    });
  });
}

$(page());

$('#back-to-top').on('click', (e) => {
  $('body,html').animate({
        scrollTop : 0                       // Scroll to top of body
    }, 500);
})

$(window).on('scroll', (e) => {
  $this = $(e.target);
  if (Math.ceil($this.scrollTop()) >= ($(document).height() - $(window).height()))
    page();
  if ($this.scrollTop() >= 500) {
    $('#back-to-top').fadeIn(300);
  } else {
    $('#back-to-top').fadeOut(300);
  }
});

function page(){
    $.ajax({
      'url': '/news/api/get',
      'type': 'GET',
      'dataType': 'json',
      'success': (response) => appendHTML(response)
    });
}
 
function appendHTML(data) {
  console.log(data)
  if (data.empty || data.error) return;

  data.forEach(card => {
    
    html = `
    <div class="content-center my-1 px-1 w-full m-auto max-w-xl md:w-1/2 lg:my-4 lg:px-4 lg:w-1/3">
    <div id="${card.pk}" class="transform hover:-translate-y-2 duration-500 ease-in-out -full md:w-3/3 p-6 flex flex-col flex-grow flex-shrink">
    <div class="flex-1 bg-white rounded-t rounded-b-none overflow-hidden shadow-lg">
    <a href="${card.fields.url}" target="_blank">`
    if (card.fields.img) {
      html += `<img src="${card.fields.img}" class="object-cover h-full w-full rounded-t">`
    }
    
    html += ` <p class="w-full text-gray-600 text-xs md:text-sm pt-6 px-6">${card.fields.outlet}</p>
    <div class="w-full font-bold text-xl text-gray-900 px-6">${card.fields.title}</div>`
    
    if (card.fields.description)
    html += `<p class="text-gray-800 mt-3 font-serif text-base px-6 mb-5">${card.fields.description}</p>`
    
    html += `
    </a>
    </div>
    </div>
    </div>
    `;
    
    $("#news-box").append(html)
  });
}
