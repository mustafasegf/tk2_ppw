from django.db import models
from django.urls import reverse
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _

# Create your models here.

# # TODO: Delete outlet class
# class Outlet(models.Model):

#     name = models.CharField(max_length=50)

#     class Meta:
#         verbose_name = _("Outlet")
#         verbose_name_plural = _("Outlets")

#     def __str__(self):
#         return self.name

#     def get_absolute_url(self):
#         return reverse("Outlet_detail", kwargs={"pk": self.pk})

class News(models.Model):

    title = models.CharField(max_length=100)
    url = models.URLField(max_length=200)
    unix_time = models.CharField(max_length=50)
    description = models.CharField(max_length=200, null=True)
    img = models.CharField(max_length=1000, null=True)
    outlet = models.CharField(max_length=1000)

    class Meta:
        verbose_name = _("News")
        verbose_name_plural = _("News")

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse("News_detail", kwargs={"pk": self.pk})

class HoaxReport(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    judul = models.CharField(max_length=100)
    text = models.TextField()
    url = models.URLField(max_length=200)
    message = models.TextField()
    
    # bookmarks = models.JSONField(blank=True)

    def __str__(self):
        return self.user.username
