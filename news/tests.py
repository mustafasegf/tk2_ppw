from django.test import TestCase
from .models import News
from .forms import HoaxForm
from login_register.forms import CreateUserForm
from django.contrib.auth import login, authenticate
from django.contrib.auth.models import User

# Create your tests here.

class TestNews(TestCase):

    def test_news_outlet_model(self):
        """
        Test for models correctness
        """

        outlet_test = {
            "name": "Dummy news outlet"
        }

        news_test = {
            "title": "This is a test",
            "url": "https://stackoverflow.com/questions/17921094/how-to-avoid-overflow-in-fast-modular-exponentiation/17921804#17921804",
            "unix_time": "1608391620479"
        }

        News.objects.create(
            title=news_test["title"],
            url=news_test["url"],
            unix_time=news_test["unix_time"],
            outlet=outlet_test
        )

        news = News.objects.get(title=news_test["title"])
        self.assertEqual(news.title, news_test["title"])
        self.assertEqual(news.url, news_test["url"])
        self.assertEqual(news.unix_time, news_test["unix_time"])

    def test_news_response(self):
        response = self.client.get('/news', follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'Lapor Hoax')

    def test_fetch_result(self):
        # response = self.client.get('/news/api/fetch', follow=True)
        response = self.client.get(
            '/news/api/fetch?title=Yogya%20Akan%20Gunakan%20GeNose%20UGM%20Secara%20Massal&url=https%3A%2F%2Ftekno.tempo.co%2Fread%2F1418331%2Fyogya-akan-gunakan-genose-ugm-secara-massal&timestamp=1609160760406',
            follow=True)
        self.assertEqual(response.status_code, 200)

    def test_login_hoax_report(self):
        response = self.client.get('/news/lapor')
        self.assertContains(response, 'Anda belum login')

        register_data = {
            "username": "test_dummy",
            "password1": "abc5dasar",
            "password2": "abc5dasar"
        }

        register_form = CreateUserForm(data=register_data)
        self.assertTrue(register_form.is_valid())
        register_form.save()

        user = authenticate(username=register_data['username'], password=register_data['password1'])
        self.client.post('/accounts/login/', {
            'username':register_data['username'], 
            'password':register_data['password1']
        })
        response = self.client.get('/news/lapor')
        self.assertContains(response, 'Melapor sebagai')

    def test_news_api_call(self):
        response = self.client.get('/news/api/get', follow=True)
        self.assertEqual(response.status_code, 200)

    def test_news_api_save(self):
        # response = self.client.get('/news/api/fetch', follow=True)
        response = self.client.get(
            '/news/api/fetch?title=Menguji%20keikhlasan%20tanpa%20batas&url=https%3A%2F%2Fwww.antaranews.com%2Fberita%2F1917764%2Fmenguji-keikhlasan-tanpa-batas&timestamp=1609166640089',
            follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.content.decode('utf-8'), 'Added')

        response = self.client.get(
            '/news/api/fetch?title=Menguji%20keikhlasan%20tanpa%20batas&url=https%3A%2F%2Fwww.antaranews.com%2Fberita%2F1917764%2Fmenguji-keikhlasan-tanpa-batas&timestamp=1609166640089',
            follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.content.decode('utf-8'), 'Skipped')

class TestHoax(TestCase):
    """
    Tests for Hoaxes
    """

    def test_form_correctness(self):
        dummy_user = User.objects.create_user(
            username='testuser',
            email='test@email.com',
            password='inisebuahdummy'
        ).save()

        submission = {
            "judul": "Test",
            "url": "https://docs.djangoproject.com/",
            "text": "Ini adalah dummy form",
            "message": "Abaikan jika tersimpan",
            "user": dummy_user
        }

        correct_form = HoaxForm(data=submission)
        self.assertTrue(correct_form.is_valid())

        submission["url"] = "invalid url"

        correct_form = HoaxForm(data=submission)
        self.assertFalse(correct_form.is_valid())

    def test_hoax_login(self):
        dummy_user = User.objects.create_user(
            username='testuser',
            email='test@email.com',
            password='inisebuahdummy'
        ).save()

        submission = {
            "judul": "Test2",
            "url": "https://djangoproject.com/",
            "text": "Ini adalah dummy form",
            "message": "Abaikan jika tersimpan",
            "user": dummy_user
        }

        response = self.client.get('/news/lapor', follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'belum login')

        self.client.post('/accounts/login/', {
            'username': 'testuser',
            'password': 'inisebuahdummy'
        })
        # self.client.login(username='testuser', password='inisebuahdummy')

        response = self.client.get('/news/lapor', follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'Melapor sebagai')

        response = self.client.post('/news/lapor', data=submission.pop('user'))
        self.assertEqual(response.status_code, 200)
