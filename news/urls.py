from django.urls import path
from . import views

app_name = 'news'

urlpatterns = [
    path('', views.index, name="index"),
    path('lapor', views.hoax, name="hoax"),
    path('api/fetch', views.fetch, name="fetch"),
    path('api/get', views.getBulk, name="news"),
]
