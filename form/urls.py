from django.urls import path

from . import views
from form.views import UserCreateView

app_name = 'form'

urlpatterns = [
    path('', UserCreateView.as_view(), name='form'),
    path('success/', views.success, name='success'),
    path('patient/', views.patient_list, name='patient_list'),
    path('details/<int:pk>', views.patient_details, name='patient_details'),
    path('delete/<int:pk>', views.delete_patient, name='delete_patient'),
    path('search/', views.search_patient, name='search_patient'),
    path('case/', views.add_case, name='add_case'),
]
