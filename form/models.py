from django.db import models


class Patient(models.Model):
    name = models.CharField(max_length=70)
    age = models.PositiveIntegerField()
    SEX_CHOICES = [
        ('Male', 'Male'),
        ('Female', 'Female'),
    ]
    sex = models.CharField(
        max_length=6,
        choices=SEX_CHOICES,
    )
    job = models.CharField(max_length=30)
    city = models.CharField(max_length=30)
    province = models.CharField(max_length=30)
    medical_history = models.CharField(max_length=80)
    education = models.CharField(max_length=30)
    symptoms = models.CharField(max_length=1000)


class Case(models.Model):
    date = models.CharField(max_length=10)
    time = models.CharField(max_length=5)
    hospital = models.CharField(max_length=30)
    doctor = models.CharField(max_length=30)
    patient_id = models.IntegerField()
