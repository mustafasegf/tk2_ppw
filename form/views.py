import json
import requests

from django.contrib import messages
from django.http import HttpResponse, JsonResponse
from django.shortcuts import render, redirect
from django.views.generic import CreateView
from django.views.decorators.csrf import csrf_exempt

from .forms import CaseForm
from .models import Patient, Case


class UserCreateView(CreateView):
    model = Patient
    template_name = 'form/form.html'
    success_url = 'success'
    fields = (
        'name',
        'age',
        'sex',
        'job',
        'city',
        'province',
        'medical_history',
        'education',
        'symptoms',
    )

    def form_valid(self, form):
        form.save()
        return super().form_valid(form)


def success(request):
    return render(request, 'form/success.html')


def patient_list(request):
    patients = Patient.objects.all()
    is_empty = False
    if not patients:
        is_empty = True
    context = {
        'patients': patients,
        'is_empty': is_empty,
    }
    return render(request, 'form/list.html', context)


def patient_details(request, pk):
    patient = Patient.objects.get(id=pk)
    isEmpty = False
    cases = Case.objects.all()
    if not cases:
        isEmpty = True
    context = {'patient': patient, 'cases': cases, 'isEmpty': isEmpty}
    return render(request, 'form/details.html', context)


def delete_patient(request, pk):
    if request.method == "POST":
        patient = Patient.objects.get(id=pk)
        patient.delete()
        messages.success(request, (f"{patient} deleted."))
    return redirect('form:patient_list')


def search_patient(request):
    query = request.GET.get('q')
    field = request.GET.get('field')
    if query:
        if field == 'name':
            results = Patient.objects.filter(name__startswith=query)
        elif field == 'age':
            results = Patient.objects.filter(age__startswith=query)
        elif field == 'sex':
            results = Patient.objects.filter(sex__startswith=query)
        elif field == 'job':
            results = Patient.objects.filter(job__startswith=query)
        elif field == 'city':
            results = Patient.objects.filter(city__startswith=query)
        elif field == 'province':
            results = Patient.objects.filter(province__startswith=query)
        elif field == 'medical_history':
            results = Patient.objects.filter(medical_history__icontains=query)
        elif field == 'education':
            results = Patient.objects.filter(education__startswith=query)
        else:
            results = Patient.objects.filter(symptoms__icontains=query)
    else:
        results = Patient.objects.all()
    data = list(results.values())
    return JsonResponse(data, safe=False)


@csrf_exempt
def add_case(request):
    if request.method == 'POST':
        data = json.loads(request.body)
        case = Case(
            date=data['date'],
            time=data['time'],
            hospital=data['hospital'],
            doctor=data['doctor'],
            patient_id=data['id'],
        )
        case.save()
        return HttpResponse(status=200)
