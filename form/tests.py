import json

from django.contrib.auth.models import User
from django.test import Client, TestCase
from django.urls import resolve, reverse

from .models import Patient, Case


class FormTest(TestCase):
    def setUp(self):
        self.client = Client()
        self.form_url = reverse('form:form')
        self.success_url = reverse('form:success')
        self.patient = Patient.objects.create(
            name='Andre',
            age=30,
            sex='Male',
            job='Petani',
            city='Depok',
            province='Jawa Barat',
            medical_history='Asma',
            education='SMA',
            symptoms='Batuk, pilek, pusing',
        )
        self.form_data = {
            'name': 'Beatrice',
            'age': 29,
            'sex': 'Female',
            'job': 'Pramugari',
            'city': 'Solo',
            'province': 'Jawa Tengah',
            'medical_history': 'Hipertensi',
            'education': 'SMP',
            'symptoms': 'Sakit kepala',
        }

    def test_form_GET(self):
        response = self.client.get(self.form_url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'form/form.html')

    def test_form_POST(self):
        response = self.client.post(self.form_url, self.form_data, follow=True)
        self.assertEqual(Patient.objects.last().name, 'Beatrice')
        self.assertTemplateUsed(response, 'form/success.html')


class LogInTest(TestCase):
    def setUp(self):
        self.client = Client()
        self.form_url = reverse('form:form')
        self.list_url = reverse('form:patient_list')
        self.login_url = '/accounts/login/'

    def test_logout_form(self):
        response = self.client.get(self.form_url)
        self.assertContains(response, 'Anda belum login')

    def test_login_form(self):
        user = User.objects.create(username='testuser')
        user.set_password('12345')
        user.save()
        self.client.post(self.login_url, {
            'username': 'testuser',
            'password': '12345'
        })
        response = self.client.get(self.form_url)
        self.assertContains(response, 'Silakan isi form')

    def test_logout_list(self):
        response = self.client.get(self.list_url)
        self.assertContains(response, 'Anda belum login')

    def test_login_list(self):
        user = User.objects.create(username='testuser')
        user.set_password('12345')
        user.save()
        logged_in = self.client.login(username='testuser', password='12345')
        response = self.client.get(self.list_url)
        self.assertContains(response, 'Search')


class PatientTest(TestCase):
    def setUp(self):
        self.client = Client()
        self.user = User.objects.create(username='testuser')
        self.user.set_password('12345')
        self.user.save()
        self.login_url = '/accounts/login/'
        self.client.post(self.login_url, {
            'username': 'testuser',
            'password': '12345'
        })
        self.patient_url = reverse('form:patient_list')
        self.patient = Patient.objects.create(
            name='Andre',
            age=30,
            sex='Male',
            job='Petani',
            city='Depok',
            province='Jawa Barat',
            medical_history='Asma',
            education='SMA',
            symptoms='Batuk, pilek, pusing',
        )
        self.details_url = reverse(
            'form:patient_details',
            args=[self.patient.id]
        )
        self.delete_patient_url = reverse(
            'form:delete_patient',
            args=[self.patient.id]
        )
        self.search_url = reverse('form:search_patient')

    def test_patient_GET(self):
        response = self.client.get(self.patient_url)
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'form/list.html')

    def test_patient_DELETE(self):
        response = self.client.post(self.delete_patient_url, follow=True)
        self.assertEquals(response.status_code, 200)
        self.assertContains(response, 'deleted')

    def test_details_GET(self):
        response = self.client.get(self.details_url, follow=True)
        self.assertEquals(response.status_code, 200)
        self.assertContains(response, 'SMA')
        self.assertTemplateUsed(response, 'form/details.html')

    def test_ajax_search_name(self):
        response = Client().get(self.search_url + '?q=Andre&field=name')
        self.assertIn('Andre', response.content.decode('utf-8'))

    def test_ajax_search_age(self):
        response = Client().get(self.search_url + '?q=30&field=age')
        self.assertIn('Andre', response.content.decode('utf-8'))

    def test_ajax_search_sex(self):
        response = Client().get(self.search_url + '?q=Male&field=sex')
        self.assertIn('Andre', response.content.decode('utf-8'))

    def test_ajax_search_job(self):
        response = Client().get(self.search_url + '?q=Petani&field=job')
        self.assertIn('Andre', response.content.decode('utf-8'))

    def test_ajax_search_city(self):
        response = Client().get(self.search_url + '?q=Depok&field=city')
        self.assertIn('Andre', response.content.decode('utf-8'))

    def test_ajax_search_province(self):
        response = Client().get(self.search_url + '?q=Jawa&field=province')
        self.assertIn('Andre', response.content.decode('utf-8'))

    def test_ajax_search_medical_history(self):
        response = Client().get(self.search_url + '?q=Asma&field=medical_history')
        self.assertIn('Andre', response.content.decode('utf-8'))

    def test_ajax_search_education(self):
        response = Client().get(self.search_url + '?q=SMA&field=education')
        self.assertIn('Andre', response.content.decode('utf-8'))

    def test_ajax_search_symptoms(self):
        response = Client().get(self.search_url + '?q=pilek&field=symptoms')
        self.assertIn('Andre', response.content.decode('utf-8'))

    def test_ajax_search_none(self):
        response = Client().get(self.search_url + '?q=&field=name')
        self.assertIn('Andre', response.content.decode('utf-8'))


class CaseTest(TestCase):
    def setUp(self):
        self.client = Client()
        self.user = User.objects.create(username='testuser')
        self.user.set_password('12345')
        self.user.save()
        self.login_url = '/accounts/login/'
        self.client.post(self.login_url, {
            'username': 'testuser',
            'password': '12345'
        })
        self.patient = Patient.objects.create(
            name='Andre',
            age=30,
            sex='Male',
            job='Petani',
            city='Depok',
            province='Jawa Barat',
            medical_history='Asma',
            education='SMA',
            symptoms='Batuk, pilek, pusing',
        )
        self.details_url = reverse(
            'form:patient_details',
            args=[self.patient.id]
        )
        self.add_case_url = reverse('form:add_case')

    def test_ajax_add_case_POST(self):
        python_dict = {
            'date': '2020-12-25',
            'time': '12:34',
            'hospital': 'RS Sehat Terus',
            'doctor': 'Tatang Sutarna',
            'id': 1
        }
        response = self.client.post(
            self.add_case_url,
            json.dumps(python_dict),
            content_type="application/json"
        )
        self.assertEquals(response.status_code, 200)
