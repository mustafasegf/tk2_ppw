$(document).ready(function () {
    $('#input-row, #save-case').hide();

    // SEARCH PATIENT
    $('#search-bar').keyup(function () {
        var query = $(this).val();
        var field = $('#field-selector option:selected').val();
        $.ajax({
            type: 'GET',
            url: '/form/search?q=' + query + '&field=' + field,
            contentType: 'application/json',
            success: function (data) {
                console.log('SEARCH SUCCESS');
                $('#list-container').empty();
                for (i = 0; i < data.length; i++) {
                    var obj = data[i];
                    var name = obj['name'];
                    var age = obj['age'];
                    var sex = obj['sex'];
                    var id = obj['id'];
                    var url = '/form/details/' + id;
                    $('#list-container').append(`
                        <div class="pb-4 bg-opacity-5 rounded-t">
                            <div class="card mt-3">
                                <div class="card-body d-flex justify-content-between flex-column flex-md-row">
                                    <div>
                                        <a href=${url}>
                                            <h5 class="card-title">${name}</h5>
                                        </a>
                                        <h6 class="card-subtitle mb-2 text-muted">${age} (${sex})</h6>
                                    </div>
                                    <form action=${url} method="POST">
                                        <button class="btn btn-danger card-link mt-2 mr-md-4 align-self-center" type="input">
                                            DELETE
                                        </button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    `);
                }
            },
            failure: function (data) {
                console.log('SEARCH FAIL');
                console.log(data);
            },
        });
    });

    // ADD PATIENT CASE
    $('#add-case').click(() => {
        $('#input-row, #save-case').show();
    });

    $('#save-case').click(() => {
        var date = $('#date-picker').val();
        var time = $('#time-picker').val();
        var hospital = $('#hospital-input').val();
        var doctor = $('#doctor-input').val();
        var id = parseInt($('#patient-id').text());

        $.ajax({
            type: 'POST',
            url: '/form/case/',
            data: JSON.stringify({
                'date': date,
                'time': time,
                'hospital': hospital,
                'doctor': doctor,
                'id': id
            }),
            dataType: 'text',
            success: function () {
                console.log('ADD CASE SUCCESS');
                $('#blank-row').before(`
                    <tr>
                        <td>${date}</td>
                        <td>${time}</td>
                        <td>${hospital}</td>
                        <td>${doctor}</td>
                    </tr>
                `);
            },
            failure: function () {
                console.log('ADD CASE FAILED');
            },
        });

        $('#input-row, #save-case').hide();
    });
});