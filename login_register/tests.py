from django.test import TestCase, Client
from django.contrib.auth.models import User
from django.test.testcases import LiveServerTestCase
from django.urls import resolve, reverse

import time
from selenium import webdriver
from selenium.webdriver.chrome.options import Options

# Create your tests here.
class UserModelTest(TestCase):
    def setUp(self):
        self.user = User.objects.create_user(
            username='testuser',
            email= 'test@email.com',
            password='12345678'
        )
    
    def test_user_count(self):
        self.assertEqual(User.objects.count(), 1)
    
    def test_user_fields(self):
        user = User.objects.create_user(
            username='danielsyahputra',
            email= 'danielsyahputra@email.com',
            password='11111111'
        )
        self.assertEqual(user.username, 'danielsyahputra')
        self.assertEqual(user.email, 'danielsyahputra@email.com')

class UrlsPathTest(TestCase):
    def setUp(self):
        self.login_page = reverse('login_register:login')
        self.logout_page = reverse('login_register:logout')
        self.signup_page = reverse('login_register:signup')

    def test_login_matching_with_the_right_path(self):
        self.assertEqual(self.login_page, '/accounts/login/')

    def test_logout_matching_with_the_right_path(self):
        self.assertEqual(self.logout_page, '/accounts/logout/')

    def test_signup_matching_with_the_right_path(self):
        self.assertEqual(self.signup_page, '/accounts/signup/')

class LoginViewTest(TestCase):
    def setUp(self):
        self.login_view = reverse('login_register:login')

    def test_login_view_exist(self):
        response = self.client.get(self.login_view)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'Login')
        self.assertTemplateUsed(response, 'registration/login.html')

class LogoutTestCase(TestCase):
    def setUp(self) -> None:
        self.login_page = reverse('login_register:login')
        self.logout_page = reverse('login_register:logout')
        self.user = User.objects.create_user(
            username='testuser',
            email= 'test@email.com',
            password='12345678'
        )

    def test_logout(self):
        self.client.post(self.login_page, {
            'username':'testuser',
            'password':'12345678'
        }, follow=True)
        response = self.client.get(self.logout_page, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'COVID - 19')
    
class LoginTestCase(TestCase):
    def setUp(self) -> None:
        self.login_page = reverse('login_register:login')
        self.user = User.objects.create_user(
            username='testuser',
            email= 'test@email.com',
            password='12345678'
        )
    
    def test_login_with_the_right_username_and_password(self):
        response = self.client.post(self.login_page, {
            'username':'testuser',
            'password':'12345678'
        }, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'COVID - 19')

    def test_login_with_the_right_username_but_wrong_password(self):
        response = self.client.post(self.login_page, {
            'username':'testuser',
            'password':'abcd'
        }, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'Wrong password, please enter the right password')

    def test_login_with_the_wrong_username_and_password(self):
        response = self.client.post(self.login_page, {
            'username':'test',
            'password':'abcd'
        }, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'Sorry, we did not recognize your account in our system')

class SignUpPageTest(TestCase):
    def setUp(self):
        self.signup_page = reverse('login_register:signup')
        self.user = User.objects.create_user(
            username='testuser',
            email= 'test@email.com',
            password='12345678'
        )

    def test_GET_signup_page(self):
        c = self.client
        response = c.get(self.signup_page)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'login_register/signup.html')
        self.assertContains(response, 'Create a new account')
    
    def test_POST_signup_page_valid(self):
        c = self.client
        response = c.post(
            self.signup_page, {
                'username': 'daniel123',
                'password1': 'awertyuh67',
                'password2': 'awertyuh67',
            }, follow=True
        )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(User.objects.count(), 2)
        self.assertContains(response, 'Login')


class LoginFunctionalTest(LiveServerTestCase):
    def setUp(self) -> None:
        super().setUp()
        chrome_options= webdriver.ChromeOptions()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        # self.selenium = webdriver.Chrome()
        self.user = User.objects.create_user(
            username='testuser',
            email= 'test@email.com',
            password='12345678'
        )

    def tearDown(self) -> None:
        self.selenium.quit()
        super().tearDown()

    def test_story9_page_title(self):
        self.selenium.get(self.live_server_url + '/accounts/login/')
        time.sleep(2)
        self.assertEqual(self.selenium.title, "Form Corona - Login")
        time.sleep(2)
        self.selenium.get(self.live_server_url + '/accounts/signup/')
        self.assertEqual(self.selenium.title, "Form Corona - Sign Up")

    def test_login_button(self):
        self.selenium.get(self.live_server_url + '/accounts/login/')
        username_box = self.selenium.find_element_by_name('username')
        password_box = self.selenium.find_element_by_name('password')
        submit_box = self.selenium.find_element_by_class_name('btn-class')
        username_box.send_keys('testuser')
        password_box.send_keys('12345678')
        submit_box.submit()
        time.sleep(4)
        self.assertEqual(self.selenium.title, "Form Corona - Home")