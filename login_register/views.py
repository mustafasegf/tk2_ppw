from django.contrib.auth.models import User
from django.http import JsonResponse
from django.shortcuts import redirect, render
from .forms import CreateUserForm
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth import authenticate, login, logout

# Create your views here.
def sign_up_page(request):
    form = CreateUserForm()
    if request.method == "POST":
        form = CreateUserForm(request.POST)
        if form.is_valid():
            form.save()
            user = form.cleaned_data.get("username")
            return redirect('login_register:login')
    context = {'form':form}
    return render(request, 'login_register/signup.html', context)

def loginView(request):
    context = {}
    if request.method == "POST":
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
        # print(user)
        # print(type(user))
        if user is not None:
            login(request, user)
            # print(status.HTTP_200_OK)
            # print("Login Successful")
            return redirect('home:index')
        else:
            try:
                user = User.objects.get(username = username)
                # print(status.HTTP_401_UNAUTHORIZED)
                # print("Wrong password, please enter the right password")
                context['message'] = "Wrong password, please enter the right password"
                return render(request, 'registration/login.html', {
                    'context' : context,
                    "form": AuthenticationForm()
                })
            except:
                context['message'] = "Sorry, we did not recognize your account in our system"
                return render(request, 'registration/login.html', {
                    'context' : context,
                    "form": AuthenticationForm()
                })
    context['message'] = 'Please login'
    return render(request, 'registration/login.html', {
            "form": AuthenticationForm()
            })

def logoutUser(request):
    logout(request)
    return redirect('home:index')