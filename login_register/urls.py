from django.urls import path

from .views import sign_up_page, loginView,  logoutUser

app_name= 'login_register'

urlpatterns = [
    path('signup/', sign_up_page, name='signup'),
    path('login/', loginView, name='login'),
    # path('loginAPI/', loginAPI, name='loginAPI'),
    path('logout/', logoutUser, name='logout')
]